package com.pearlchain.test.repository;

import com.pearlchain.test.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * Репозиторий для хранения данных из базы данных.
 */
@Repository
@Transactional
public interface ProductRepository extends CrudRepository<Product, Long> {

	Set<Product> findAll();

	Product findProductById(Long id);
}
