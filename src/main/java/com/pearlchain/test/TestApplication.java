package com.pearlchain.test;

import com.pearlchain.test.logic.CommandLineListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestApplication.class, args);

		// run the command line listener thread
		new CommandLineListener().start();
	}
}