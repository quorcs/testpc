package com.pearlchain.test.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.pearlchain.test.attribute.*;
import com.pearlchain.test.entity.*;
import com.pearlchain.test.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Класс реализует интерфейс ProductService
 * для работы с данными в базе данных и файле CSV.
 */
@Service
public class ProductServiceImpl implements ProductService {

	private final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);

	private static final ObjectMapper MAPPER = new ObjectMapper();

	// путь к дирректории с файлом по умолчанию
	private String filePath = "src/main/resources/";

	private final ProductRepository productRepository;

	@Autowired
	public ProductServiceImpl(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@Override
	public Set<Product> getAll() {
		return productRepository.findAll();
	}

	@Override
	public Product getById(Long id) {
		return productRepository.findProductById(id);
	}

	/**
	 * Экспорт данных из базы данных в файл
	 * @param fileName имя файла, либо путь к файлу
	 */
	@Override
	public void loadToFile(String fileName) {
		CSVWriter csvWriter;

		try {
			csvWriter = new CSVWriter(new FileWriter(getFilePath(fileName)));

			List<String[]> list = new ArrayList<>();

			for (Product product : productRepository.findAll()) {
				String name = null;
				String attribute = null;

				// определяем имя типа продукта и его атрибут
				if (product.getClass().equals(Computer.class)) {
					Computer computer = (Computer) product;
					name = Computer.class.getSimpleName();
					attribute = MAPPER.writeValueAsString(new FormFactor(computer.getFormFactor()));
				} else if (product.getClass().equals(Laptop.class)) {
					Laptop laptop = (Laptop) product;
					name = Laptop.class.getSimpleName();
					attribute = MAPPER.writeValueAsString(new Scale(laptop.getScale()));
				} else if (product.getClass().equals(Monitor.class)) {
					Monitor monitor = (Monitor) product;
					name = Monitor.class.getSimpleName();
					attribute = MAPPER.writeValueAsString(new Diagonal(monitor.getDiagonal()));
				} else if (product.getClass().equals(HDD.class)) {
					HDD hdd = (HDD) product;
					name = HDD.class.getSimpleName();
					attribute = MAPPER.writeValueAsString(new Capacity(hdd.getCapacity()));
				}

				if (name != null && attribute != null) {
					list.add(new String[]{
							name,
							MAPPER.writeValueAsString(product.getCount()),
							MAPPER.writeValueAsString(new SerialNumber(product.getSerialNumber())),
							MAPPER.writeValueAsString(product.getProducer()),
							MAPPER.writeValueAsString(new Price(product.getPrice())),
							attribute
					});
				} else {
					LOGGER.error("The variable name or attribute is null. Please to check it.");
				}
			}

			csvWriter.writeAll(list);
			csvWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Импорт данных из файла в базу данных.
	 * Если позиция файла совпадает с позицией базы данных,
	 * то в базе инкрементируется количество продуктов такой позиции.
	 * Иначе создается новая позиция.
	 * @param fileName имя файла, либо путь к файлу
	 */
	@Override
	public void loadFromFile(String fileName) {
		CSVReader csvReader;

		try {
			csvReader = new CSVReader(new FileReader(getFilePath(fileName)));

			List<String[]> records = csvReader.readAll();

			for (String[] record : records) {
				SerialNumber serialNumber = MAPPER.readValue(record[2], new TypeReference<SerialNumber>(){});
				Price price = MAPPER.readValue(record[4], new TypeReference<Price>(){});
				Producer producer = MAPPER.readValue(record[3], new TypeReference<Producer>(){});

				if (record[0].equals(Computer.class.getSimpleName())) {
					FormFactor formFactor = MAPPER.readValue(record[5], new TypeReference<FormFactor>(){});

					// обработка строки файла
					checkProduct(new Computer(
							Integer.parseInt(record[1]),
							serialNumber.getSerialNumber(),
							producer,
							price.getPrice(),
							formFactor.getFormFactor()
					));
				} else if (record[0].equals(Laptop.class.getSimpleName())) {
					Scale scale = MAPPER.readValue(record[5], new TypeReference<Scale>(){});

					// обработка строки файла
					checkProduct(new Laptop(
							Integer.parseInt(record[1]),
							serialNumber.getSerialNumber(),
							producer,
							price.getPrice(),
							scale.getScale()
					));
				} else if (record[0].equals(Monitor.class.getSimpleName())) {
					Diagonal diagonal = MAPPER.readValue(record[5], new TypeReference<Diagonal>(){});

					// обработка строки файла
					checkProduct(new Monitor(
							Integer.parseInt(record[1]),
							serialNumber.getSerialNumber(),
							producer,
							price.getPrice(),
							diagonal.getDiagonal()
					));
				} else if (record[0].equals(HDD.class.getSimpleName())) {
					Capacity capacity = MAPPER.readValue(record[5], new TypeReference<Capacity>(){});

					// обработка строки файла
					checkProduct(new HDD(
							Integer.parseInt(record[1]),
							serialNumber.getSerialNumber(),
							producer,
							price.getPrice(),
							capacity.getCapacity()
					));
				}
			}
		} catch (IOException e) {
			LOGGER.warn("No such file exist. Please to create the file and try again.");
		}
	}

	/**
	 * Определяем путь к файлу.
	 * Если в пути содержится слэш или обратный слэш,
	 * то это полный путь к файлу и мы его берем полностью.
	 * Если же fileName - только имя файла, то добавляем
	 * дирректорию по умолчанию.
	 * @param fileName имя файла
	 */
	private String getFilePath(String fileName) {
		if (fileName.contains("/") || fileName.contains("\\")) {
			return fileName;
		} else {
			return filePath + fileName;
		}
	}

	/**
	 * Обработка строки файла.
	 * Если позиция в базе данных совпадает со строкой,
	 * то инкрементируем количество в базе.
	 * Иначе добавляем новую позицию в базу данных.
	 * @param product проверяемый продукт
	 */
	private void checkProduct(Product product) {
		for (Product p : productRepository.findAll()) {
			if (p.equals(product)) {
				p.setCount(p.getCount() + product.getCount());
				productRepository.save(p);
			} else {
				productRepository.save(product);
			}
		}
	}
}
