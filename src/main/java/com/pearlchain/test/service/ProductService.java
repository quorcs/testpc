package com.pearlchain.test.service;

import com.pearlchain.test.entity.Product;

import java.util.Set;

public interface ProductService {

	Set<Product> getAll();

	Product getById(Long id);

	void loadToFile(String fileName);

	void loadFromFile(String fileName);
}
