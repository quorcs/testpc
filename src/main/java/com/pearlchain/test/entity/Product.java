package com.pearlchain.test.entity;

import com.opencsv.bean.CsvBindByPosition;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "products")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "p_type")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@CsvBindByPosition(position = 0)
	@Column(name = "serial_number")
	private Long serialNumber;

	@CsvBindByPosition(position = 1)
	@ManyToOne
	@JoinColumn(name = "producer_id")
	private Producer producer;

	@CsvBindByPosition(position = 2)
	private Double price;

	@CsvBindByPosition(position = 3)
	private Integer count;

	public Product(Long serialNumber, Producer producer, Double price, Integer count) {
		this.serialNumber = serialNumber;
		this.producer = producer;
		this.price = price;
		this.count = count;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Product)) return false;

		Product product = (Product) o;

		return (serialNumber != null ? serialNumber.equals(product.serialNumber) : product.serialNumber == null) &&
				(producer != null ? producer.equals(product.producer) : product.producer == null) &&
				(price != null ? price.equals(product.price) : product.price == null);
	}

	@Override
	public int hashCode() {
		int result = serialNumber != null ? serialNumber.hashCode() : 0;
		result = 31 * result + (producer != null ? producer.hashCode() : 0);
		result = 31 * result + (price != null ? price.hashCode() : 0);
		return result;
	}
}
