package com.pearlchain.test.entity;

import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@DiscriminatorValue("HDD")
public class HDD extends Product {

	@CsvBindByPosition(position = 4)
	private Integer capacity;

	public HDD(Integer count, Long serialNumber, Producer producer, Double price, Integer capacity) {
		super(serialNumber, producer, price, count);
		this.capacity = capacity;
	}

	@Override
	public String toString() {
		return "HDD{" +
				"serialNumber=" + getSerialNumber() +
				", producer=" + getProducer() +
				", price=" + getPrice() +
				", count=" + getCount() +
				", capacity=" + getCapacity() +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof HDD)) return false;
		if (!super.equals(o)) return false;

		HDD hdd = (HDD) o;

		return capacity != null ? capacity.equals(hdd.capacity) : hdd.capacity == null;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (capacity != null ? capacity.hashCode() : 0);
		return result;
	}
}
