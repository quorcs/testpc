package com.pearlchain.test.entity;

import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@DiscriminatorValue("LAP")
public class Laptop extends Product {

	@CsvBindByPosition(position = 5)
	private Double scale;

	public Laptop(Integer count, Long serialNumber, Producer producer, Double price, Double scale) {
		super(serialNumber, producer, price, count);
		this.scale = scale;
	}

	@Override
	public String toString() {
		return "Laptop{" +
				"serialNumber=" + getSerialNumber() +
				", producer=" + getProducer() +
				", price=" + getPrice() +
				", count=" + getCount() +
				", scale=" + getScale() +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Laptop)) return false;
		if (!super.equals(o)) return false;

		Laptop laptop = (Laptop) o;

		return scale != null ? scale.equals(laptop.scale) : laptop.scale == null;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (scale != null ? scale.hashCode() : 0);
		return result;
	}
}
