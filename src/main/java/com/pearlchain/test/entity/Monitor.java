package com.pearlchain.test.entity;

import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@DiscriminatorValue("MON")
public class Monitor extends Product {

	@CsvBindByPosition(position = 4)
	private Double diagonal;

	public Monitor(Integer count, Long serialNumber, Producer producer, Double price, Double diagonal) {
		super(serialNumber, producer, price, count);
		this.diagonal = diagonal;
	}

	@Override
	public String toString() {
		return "Monitor{" +
				"serialNumber=" + getSerialNumber() +
				", producer=" + getProducer() +
				", price=" + getPrice() +
				", count=" + getCount() +
				", diagonal=" + getDiagonal() +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Monitor)) return false;
		if (!super.equals(o)) return false;

		Monitor monitor = (Monitor) o;

		return diagonal != null ? diagonal.equals(monitor.diagonal) : monitor.diagonal == null;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (diagonal != null ? diagonal.hashCode() : 0);
		return result;
	}
}
