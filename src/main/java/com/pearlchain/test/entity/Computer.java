package com.pearlchain.test.entity;

import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@DiscriminatorValue("COM")
public class Computer extends Product {

	@CsvBindByPosition(position = 4)
	@Column(name = "form_factor")
	private String formFactor;

	public Computer(Integer count, Long serialNumber, Producer producer, Double price, String formFactor) {
		super(serialNumber, producer, price, count);
		this.formFactor = formFactor;
	}

	@Override
	public String toString() {
		return "Computer{" +
				"serialNumber=" + getSerialNumber() +
				", producer=" + getProducer() +
				", price=" + getPrice() +
				", count=" + getCount() +
				", formFactor=" + getFormFactor() +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Computer)) return false;
		if (!super.equals(o)) return false;

		Computer computer = (Computer) o;

		return formFactor != null ? formFactor.equals(computer.formFactor) : computer.formFactor == null;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (formFactor != null ? formFactor.hashCode() : 0);
		return result;
	}
}
