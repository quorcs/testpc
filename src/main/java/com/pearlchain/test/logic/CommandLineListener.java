package com.pearlchain.test.logic;

import com.pearlchain.test.ApplicationContextHolder;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;

/**
 * Класс-слушатель для командной строки
 * Обрабатывает ввод текста с клавиатуры
 * Реализует возможности:
 * - прекратить работу приложения
 * - загрузить данные из файла
 * - выгрузить данные в файл
 * - показать справку
 */
public class CommandLineListener extends Thread {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommandLineListener.class);

	private Scanner scanner = new Scanner(System.in);

	public CommandLineListener() {
		setDaemon(true);
	}

	@Override
	public void run() {
		Options options = new Options()
				.addOption("h", false, "Help")
				.addOption("import", true, "Import from file: import <fileName>")
				.addOption("export", true, "Export to file: export <fileName>")
				.addOption("exit", false, "Terminate the application");

		CSVManager csvManager = (CSVManager) ApplicationContextHolder.getApplicationContext().getBean("CSVManager");

		while (true) {
			String[] s = scanner.nextLine().split(" ");

			try {
				CommandLine commandLine = new DefaultParser().parse(options, s);

				if (commandLine.hasOption("h") || s.length == 0) {
					printHelp(options);
				}

				if (commandLine.hasOption("exit")) {
					LOGGER.trace("Terminate the application");
					System.exit(1);
				}

				if (commandLine.hasOption("import")) {
					csvManager.loadFromFile(commandLine.getOptionValue("import"));
				}

				if (commandLine.hasOption("export")) {
					csvManager.loadToFile(commandLine.getOptionValue("export"));
				}
			} catch (ParseException e) {
				LOGGER.warn("No such command format. Please try again.");
			}
		}
	}

	/**
	 * Вывод помощи по опциям в консоль
	 * @param options опции для запуска приложения
	 */
	private static void printHelp(Options options) {
		LOGGER.trace("Print help");
		String header = "\nEnter some parameters\n\n";
		String footer = "\nPlease report issues at valtario349@gmail.com";

		HelpFormatter helpFormatter = new HelpFormatter();
		helpFormatter.printHelp("Test task for PearlChain", header, options, footer, true);
	}
}
