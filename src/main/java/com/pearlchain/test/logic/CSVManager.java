package com.pearlchain.test.logic;

import com.pearlchain.test.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Менеджер для управления экспортом в файл и импортом из файла.
 */
@Component
public class CSVManager {

	private final ProductService productService;

	@Autowired
	public CSVManager(ProductService productService) {
		this.productService = productService;
	}

	void loadToFile(String fileName) {
		productService.loadToFile(fileName);
	}

	void loadFromFile(String fileName) {
		productService.loadFromFile(fileName);
	}
}
